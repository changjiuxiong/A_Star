class Node{
    constructor(x,y) {
        this.x = x;
        this.y = y;

        this.g = 0;
        this.h = 0;
        this.f = 0;
        this.parent = null;

    }

    initF(endNode){

        let g0 = this.getG(this, this.parent);
        this.g = this.parent.g + g0;

        //对角距离
        let dx = Math.abs(this.x - endNode.x);
        let dy = Math.abs(this.y - endNode.y);
        let maxD = Math.max(dx, dy);
        let minD = Math.min(dx, dy);
        this.h = Math.SQRT2 * minD + (maxD - minD);

        // this.h = Math.sqrt(dx*dx + dy*dy);

        this.f = this.g + this.h;
    }

    getG(n1, n2){
        let g0 = Math.SQRT2;
        if(n1.x === n2.x || n1.y === n2.y){
            g0 = 1;
        }
        return g0;
    }

    equals(node){
        if(node.x === this.x && node.y === this.y){
            return true;
        }
        return false;
    }

}

export default Node;