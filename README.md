# A_Star

#### 介绍
A*寻路算法

#### 安装教程

1.npm install  
2.npm run dev  
3.打开index.html  
4.自定义地图：example/drawMap.html  

#### 使用说明

  let task = new AStar.Task();  //新建寻路任务  
  task.setStart(40,0);   //设置起点  
  task.setEnd(40,80);    //设置终点  

  //设置障碍/墙  
  task.setWallFromArray([  
      39,40,  
      40,40,  
      41,40,  
  ]);  
  task.findPath();  //找到路径  
  console.log(task.pathSet);  //输出路径结果  

![运行截图](https://images.gitee.com/uploads/images/2020/0730/105954_bccb2a75_2665180.png "屏幕截图.png")  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0801/121230_e7f64c9c_2665180.png "121047.png")  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
